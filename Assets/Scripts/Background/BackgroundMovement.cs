﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundMovement : MonoBehaviour {

    public float speed = 2;
    public bool randomSpeed = false;
    public float lowerSpeed = 1f;
    public float higherSpeed = 5f;

    public int offset = 4;
    Vector3 position;
    SpriteRenderer sprite;

    void Start()
    {
        position = Camera.main.ScreenToWorldPoint(new Vector3(0, Camera.main.pixelHeight, Camera.main.nearClipPlane));
        sprite = GetComponent<SpriteRenderer>();
        RandomMovement();
    }

    // Update is called once per frame
    void Update ()
    {
        Move();
    }

    /// <summary>
    /// Move background. If background is out of left corner
    /// then flips it to right
    /// </summary>
    public void Move()
    {
        transform.position += Vector3.left * speed * Time.deltaTime;
        if (transform.position.x < position.x - offset / 2)
        {
            transform.position = new Vector3(-position.x + offset, transform.position.y);
        }
    }

    /// <summary>
    /// Set random speed of gameobject
    /// </summary>
    public void RandomMovement()
    {
        if (randomSpeed)
        {
            if (lowerSpeed < higherSpeed)
            {
                speed = Random.Range(lowerSpeed, higherSpeed);
            } else
            {
                throw new System.Exception("Lower speed should be smaller then higher one");
            }
        }
    }
}
