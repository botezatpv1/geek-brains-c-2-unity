﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Background : MonoBehaviour {

    public GameObject backgroundTile;
    public GameObject[] stars;

    public int starNumbers = 200;

    Vector3 screenPos;
    // Use this for initialization
    void Start()
    {
        screenPos = Camera.main.ScreenToViewportPoint(transform.position);
        SpawnBackgroundTiles();
        SpawnStars();
    }

    /// <summary>
    /// Spawns background tiles
    /// </summary>
    void SpawnBackgroundTiles()
    {
        for (int i = -5; i < 5; i++)
        {
            for (int y = -1; y < 3; y++)
            {
                GameObject _backgroundTile = Instantiate(backgroundTile, screenPos + new Vector3(i * 3, y * 3), Quaternion.identity) as GameObject;
                _backgroundTile.transform.parent = transform;
                _backgroundTile.name = "Tile" + i + "," + y;
            }
        }
    }

    /// <summary>
    /// Spawns stars
    /// </summary>
    void SpawnStars()
    {
        for (int i = 0; i < starNumbers; i++)
        {
            int starIndex = Random.Range(0, stars.Length);
            GameObject _star = Instantiate(stars[starIndex], screenPos + new Vector3(Random.Range(-10f, 10f), Random.Range(-6f, 6f)), Quaternion.identity) as GameObject;
            _star.transform.parent = transform;
        }
    }


}
