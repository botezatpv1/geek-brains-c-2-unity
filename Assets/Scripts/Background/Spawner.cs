﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    public float width = 5f;
    public float height = 10f;

    public GameObject[] meteor;

    bool RaisedDifficulty;

    private void Start()
    {
        Spawn();
    }

    /// <summary>
    /// Spawns meteors at a rate
    /// </summary>
    void Spawn()
    {
        InvokeRepeating("CreateMeteor", Time.deltaTime, 0.5f);
    }

    /// <summary>
    /// Displays in unity bounds of spawn area
    /// </summary>
    public void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(transform.position, new Vector3(width, height));
    }

    /// <summary>
    /// Creates meteor
    /// </summary>
    public void CreateMeteor()
    {
        Vector3 spawnPosition = new Vector3(Random.Range(-width / 2, width / 2) + transform.position.x, Random.Range(-height / 2, height / 2), 0);
        Instantiate(meteor[Random.Range(0, meteor.Length)], spawnPosition, Quaternion.identity);
    }
}
