﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Enemy : MonoBehaviour, IEnemy
{
    void Start()
    {
        InitEnemy();
        AddMovement();
    }

    private void Update()
    {
        AddRotation();
    }

    /// <summary>
    /// Initialize all needed components
    /// </summary>
    public abstract void InitEnemy();

    /// <summary>
    /// Adds rotation to enemy
    /// </summary>
    public virtual void AddRotation() { }

    /// <summary>
    /// Adds velocity to enemy
    /// </summary>
    public abstract void AddMovement();

    /// <summary>
    /// Checks if enemy collided with some objects
    /// </summary>
    /// <param name="collision">Object which collided with enemy</param>
    public abstract void OnTriggerEnter2D(Collider2D collision);
}
