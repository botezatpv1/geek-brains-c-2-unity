﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meteor : Enemy {

    Rigidbody2D rb;
    Animator animator;

    public int xMinimumVelocity = -4;
    public int xMaxmimumVelocity = 4;
    public int yMinimumVelocity = -2;
    public int yMaxmimumVelocity = 2;
    public int destroyScore = 10;

    float turnoverInMinute = 10;
    bool destroyed = false;

    public AudioSource audioSource;
    public AudioClip BoomClip;

    public GameObject[] powerUps;

    public override void InitEnemy()
    {
        rb = gameObject.AddComponent<Rigidbody2D>();
        rb.isKinematic = true;
        audioSource = gameObject.AddComponent<AudioSource>();
        animator = GetComponent<Animator>();
    }

    /// <summary>
    /// Makes meteor rotate at particular speed
    /// </summary>
    public override void AddRotation()
    {
        transform.Rotate(0, 0, 6 * turnoverInMinute * Time.deltaTime);
    }

    /// <summary>
    /// Makes meteor move most his way on screen
    /// </summary>
    public override void AddMovement()
    {
        int xVelocity = 0;
        if (xMinimumVelocity < xMaxmimumVelocity)
        {
            xVelocity = Random.Range(-xMaxmimumVelocity, -xMinimumVelocity);
        } else
        {
            throw new System.Exception("xMinimum should be smaller then xMaximum");
        }

        int yVelocity = 0;
        if (yMinimumVelocity < yMaxmimumVelocity)
        {
            yVelocity = Random.Range(yMinimumVelocity, yMaxmimumVelocity);
        } else
        {
            throw new System.Exception("yMinimum should be smaller then yMaximum");
        }

        turnoverInMinute = Mathf.Abs(xVelocity + yVelocity / 2);
        //if ((transform.position.y > 0 && yVelocity > 0) || (transform.position.y < 0 && yVelocity < 0))
        //{
        //    yVelocity = -yVelocity;
        //}
        rb.velocity = new Vector3(xVelocity * 100, yVelocity * 100) * Time.deltaTime;
    }


    /// <summary>
    /// If meteor collided with some object
    /// Destroys it and play destroy animation
    /// </summary>
    /// <param name="collision"></param>
    public override void OnTriggerEnter2D(Collider2D collision)
    {
        if (!destroyed)
        {
            if (collision.gameObject.tag == "PlayerAttack" || collision.gameObject.tag == "Player")
            {
                audioSource.PlayOneShot(BoomClip);
                animator.SetTrigger("Die");
                rb.velocity = -rb.velocity / 4;
                Destroy(gameObject, animator.GetCurrentAnimatorStateInfo(0).length);
                Player.Scores += destroyScore;
            }
            else
            {
                Destroy(gameObject);
            }

            if (collision.gameObject.tag == "PlayerAttack")
            {
                int? powerUpIndex = Utils.SpawnRandomItem(powerUps, 0.06f);
                if (powerUpIndex != null)
                {
                    Instantiate(powerUps[(int)powerUpIndex], transform.position, Quaternion.identity);
                }
            }
            destroyed = true;
        }
    }
}
