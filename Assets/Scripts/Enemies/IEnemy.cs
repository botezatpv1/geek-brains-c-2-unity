﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEnemy
{
    /// <summary>
    /// Initialize all needed components
    /// </summary>
    void InitEnemy();

    /// <summary>
    /// Adds velocity to enemy
    /// </summary>
    void AddMovement();

    /// <summary>
    /// Checks if enemy collided with some objects
    /// </summary>
    /// <param name="collision">Object which collided with enemy</param>
    void OnTriggerEnter2D(Collider2D collision);
}
