﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SpaceShip : Player
{
    public AudioClip ShootClip;
    public GameObject Laser;

    public static int numberOfLasers = 1;
    int maxNumberOfLasers = 3;
    /// <summary>
    /// Creates laser when player push space button
    /// </summary>
    public override void CreateMissile()
    {
        List<float> laserPositions = new List<float> { 0f, 0.5f, -0.5f };
        audioSource.PlayOneShot(ShootClip);
        if (numberOfLasers > maxNumberOfLasers) numberOfLasers = maxNumberOfLasers;
        for (int i = 0; i < numberOfLasers; i++)
        {
            int modifier = 0;
            if (numberOfLasers == 2) modifier = 1;
            Instantiate(Laser, transform.position + new Vector3(1, laserPositions[i + modifier], 0), transform.rotation);
        }
    }
}
