﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public abstract class Player : MonoBehaviour, IPlayer
{
    public float speed;

    public static float firingRate = 0.5f;
    float maxFiringRate = 0.1f;

    public static int Lives = 16;
    public static int Scores = 0;

    [SerializeField]
    private int _Lives = 3;

    [SerializeField]
    private float _firingRate = 0.5f;

    float offset = 0.5f;
    Vector3 position;
    Camera cam;
    Rigidbody2D rb;
    public AudioSource audioSource;

    // Use this for initialization
    void Start()
    {
        InitPlayer();
        DebugLog<string> debugLog = ShowPlayerStats;
    }

    // Update is called once per frame
    void Update()
    {
        Move();
        Fire();
    }

    /// <summary>
    /// Gets proper player position and adds rigidBody to him
    /// </summary>
    public void InitPlayer()
    {
        cam = Camera.main;
        rb = gameObject.AddComponent<Rigidbody2D>();
        audioSource = gameObject.AddComponent<AudioSource>();
        rb.isKinematic = true;
        Lives = _Lives;
        firingRate = _firingRate;
        Scores = 0;
        position = cam.ScreenToWorldPoint(new Vector3(0, cam.pixelHeight, cam.nearClipPlane));
    }

    /// <summary>
    /// Based on input moves player
    /// Player can not exceed screen
    /// </summary>
    public void Move()
    {
        if (Input.GetKey(KeyCode.UpArrow))
        {
            transform.position += Vector3.up * speed * Time.deltaTime;
        }
        else if (Input.GetKey(KeyCode.DownArrow))
        {
            transform.position += Vector3.down * speed * Time.deltaTime;
        }
        float yPosition = Mathf.Clamp(transform.position.y, -position.y + offset, position.y - offset);
        transform.position = new Vector3(transform.position.x, yPosition);
    }

    /// <summary>
    /// Creates missiles with firing rate when player press space bar
    /// </summary>
    public void Fire()
    {
        if (firingRate < maxFiringRate)
        {
            firingRate = maxFiringRate;
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            InvokeRepeating("CreateMissile", Time.deltaTime, firingRate);
        }
        else if (Input.GetKeyUp(KeyCode.Space))
        {
            CancelInvoke();
        }
    }

    /// <summary>
    /// Is used for instantiating different types of missiles
    /// </summary>
    public abstract void CreateMissile();

    /// <summary>
    /// If player collided with enemy remove one of his lives
    /// If player dies move to game over scene
    /// </summary>
    /// <param name="collider"></param>
    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.tag != "PowerUp")
        {
            if (Lives > 1)
            {
                Lives -= 1;
            }
            else
            {
                SceneManager.LoadScene("GameOver");
            }
        }
        ShowPlayerStats("On trigger");
    }

    public void ShowPlayerStats(string info)
    {
        Debug.Log("Player Lives: " + Lives + "; Player Firing rate: " + firingRate + "; Additinal info: " + info);
    }
}
