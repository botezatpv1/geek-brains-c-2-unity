﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPlayer {
    /// <summary>
    /// Player fiering method
    /// </summary>
    void Fire();

    /// <summary>
    /// Player moving method
    /// </summary>
    void Move();

    /// <summary>
    /// Player Initialization method
    /// </summary>
    void InitPlayer();
}
