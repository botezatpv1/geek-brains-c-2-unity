﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPill : PowerUp {

	public override void Effect()
    {
        Player.Lives += 1;
    }
}
