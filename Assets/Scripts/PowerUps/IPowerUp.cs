﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPowerUp {
    void InitPowerUp();
    void Effect();
    void AddMovement();
}
