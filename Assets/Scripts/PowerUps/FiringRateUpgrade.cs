﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FiringRateUpgrade : PowerUp {

    public override void Effect()
    {
        Player.firingRate -= 0.1f;
    }
}
