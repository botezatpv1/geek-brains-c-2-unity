﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserUpgrade : PowerUp
{
    public override void Effect()
    {
        SpaceShip.numberOfLasers += 1;
    }
}
