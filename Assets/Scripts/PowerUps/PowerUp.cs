﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PowerUp : MonoBehaviour, IPowerUp {

    public float dropRate = 0.1f;

    Rigidbody2D rb;
    AudioSource audioSource;
    public AudioClip powerUpClip;


    void Start()
    {
        InitPowerUp();
        AddMovement();
        DebugLog<string> debugLog = ShowPowerUpStats;
    }

    /// <summary>
    /// Adds rigidbody2d and audioSource to power up
    /// </summary>
    public void InitPowerUp()
    {
        rb = gameObject.AddComponent<Rigidbody2D>();
        rb.isKinematic = true;
        audioSource = gameObject.AddComponent<AudioSource>();
    }

    public void AddMovement()
    {
        rb.velocity = new Vector2(-3f, 0f);
    }

    /// <summary>
    /// Trigger Effect(), play audioclip and destroy powerUp if
    /// power up collided with player
    /// </summary>
    /// <param name="collider"></param>
    private void OnTriggerEnter2D(Collider2D collider)
    {

        if (collider.gameObject.tag == "Player")
        {
            audioSource.PlayOneShot(powerUpClip);
            Effect();
            Destroy(gameObject, powerUpClip.length);
        }
        else if (collider.gameObject.tag == "Border")
        {
            Destroy(gameObject);
        }
        ShowPowerUpStats(gameObject.name);


    }

    public void ShowPowerUpStats(string info)
    {
        Debug.Log("Power uped by" + info);
    }

    /// <summary>
    /// 
    /// </summary>
    public abstract void Effect();

}
