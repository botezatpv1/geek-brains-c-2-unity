﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerLives : MonoBehaviour {

    public Image LiveIcon;

    private void Update()
    {
        ShowPlayerLives();
    }

    /// <summary>
    /// Properly displays current number of player lives in UI
    /// </summary>
    public void ShowPlayerLives()
    {
        if (transform.childCount > Player.Lives)
        {
            Transform img = transform.GetChild(transform.childCount - 1);
            Destroy(img.gameObject);
        }
        else if (transform.childCount < Player.Lives)
        {
            for (int i = transform.childCount; i < Player.Lives; i++)
            {
                Image img = Instantiate(LiveIcon, transform);
                img.rectTransform.position = new Vector3(img.rectTransform.position.x + i * 40, img.rectTransform.position.y, 0);
            }
        }
    }
}
