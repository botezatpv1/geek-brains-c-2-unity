﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ui : MonoBehaviour {

    public Text Scores;

	// Update is called once per frame
	void Update () {
        GetScores();
    }

    /// <summary>
    /// Updates player scores
    /// </summary>
    public void GetScores()
    {
        Scores.text = "Scores: " + Player.Scores;
    }
}
