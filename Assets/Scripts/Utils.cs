﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void DebugLog<T>(T debugMessage);

public static class Utils
{
    /// <summary>
    /// Returns index of item to be spawned or null based on probability
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="spawnableItems">Array of objects to be spawned</param>
    /// <param name="spawnRate">Probability of spawning from 0 to 1</param>
    /// <returns></returns>
    public static int? SpawnRandomItem<T>(T[] spawnableItems, float spawnRate)
    {
        float spawn = Random.Range(0f, 1f);
        if (spawn < spawnRate)
        {
            return Random.Range(0, spawnableItems.Length);
        } else
        {
            return null;
        }
    }
}
